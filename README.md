# hwtest: A self-test and auto-grader tool for programming assignments.

`hwtest.py` is a test/grading tool originally built for ECE/CS 250. Features:
  - Supports running executables, Java programs, Logisim circuits (using logisim_ev_cli -- also developed in-house), and SPIM programs
  - Runs under both Python 2 and Python 3 under a variety of conditions
  - Compatible with GradeScope automation
  - Designed to be flexible and hence likely adaptable to other courses/purposes

## Usage

The tool requires a test directory with a settings.json to operate. It defaults to looking for 'tests' under the current directory. In this directory is a file 'settings.json' that dictates the tests. *Expected output* will be in this directory as well, and the tool will generate *actual output* and a *diff* between expected and actual in the directory as well.

A small toy set of programs and tests is included.

Run without arguments for usage:

    usage: hwtest.py [-h] [-C] [-v] [-t TESTDIR] <SUITE_NAME>

    Student auto-tester version 3.0.0.

    positional arguments:
    <SUITE_NAME>   A test suite name to run ('example-echo', 'example-add'), or
                    'ALL' for all of them.

    optional arguments:
    -h, --help     show this help message and exit
    -C, --clean    Remove generated actual and diff files for chosen suite(s).
    -v, --verbose  Verbose mode. Shows the commands executed.
    -t TESTDIR     Choose the directory with the tests and test content.
                    Default: tests

There's also an additional hidden `-G` option to generate the expected results. Use this with the instructor solution.

Normal use is to provide a test suite name (or 'ALL' for all of them):

    $ ./hwtest.py ALL
    Running tests for example-echo...
    Test 0     No arguments                                       Pass
    Test 1     'hi'                                               Pass
    Done running tests for example-echo.

    Running tests for example-add...
    Test 0     No arguments                                       Pass
    Test 1     1+1                                                Pass
    Test 2     2+3                                                Pass
    Done running tests for example-add.

You can change the tests directory with `-t`; this is commonly useful to switch to becoming an instructor auto-grader rather than a student self-tester. An example grader setup is provided in `tests-autograder/`:

    $ ./hwtest.py -t tests-autograder/ ALL
    Running tests for example-echo...
    Test 0     No arguments                                       Pass   2.00/2.00
    Test 1     'hi'                                               Pass   2.00/2.00
    Test 2     Long argument                                      Pass   3.00/3.00
    Test 3     Many arguments                                     Pass   3.00/3.00
    Done running tests for example-echo.

    Running tests for example-add...
    Test 0     No arguments                                       Pass   2.00/2.00
    Test 1     1+1                                                Pass   2.00/2.00
    Test 2     2+3                                                Pass   2.00/2.00
    Test 3     11+29                                              Pass   3.00/3.00
    Test 4     Just 1                                             Pass   3.00/3.00
    Test 5     Too many args                                      Pass   3.00/3.00
    Done running tests for example-add.

    Done. Score: 25.00 / 25.00

When run with in grader configuration, the tool automatically produces a `results.json` compatible with GradeScope's automated programming assignment system.

## Configuration

Behavior of the tool is based on the settings.json found in the test directory. Here's the basic supplied one supplemented with comments:

    {
      "mode": "exe",                                < Executable program mode
      "penalty_exitcode_nonzero": 0.75,             < Penalize programs that don't exit cleanly
      "test_suites": {                              < List of test suites
        "example-echo": {                           < Test suite name is the program name by default
          "tests": [
            { "desc": "No arguments",  "args": []}, < Individual tests ...
            { "desc": "'hi'",  "args": ["hi"]}
          ]
        },
        "example-add": {                            < A second suite for the other program
          "tests": [
            { "desc": "No arguments",  "args": []},
            { "desc": "1+1",  "args": ["1","1"]},
            { "desc": "2+3",  "args": ["2","3"]}
          ]
        }
      }
    }

The system is fully documented below, but honestly, seeing a bunch of example configs is the best thing. These aren't provided here, as they include confidential test cases from ECE/CS 250, but contact Tyler Bletsch if you'd like to see reference assignments.

### All the options

Top-level options:
  - `"is_grader"`: Boolean (true or false) -- must be set to true for auto-grading. Optional, default: false. 
  - `"mode"`: One of the set {"exe", "spim", "logisim", "java"}. Required.
      - `"exe"`: Runs compiled programs. Program name is the suite name unless overridden with "target".
      - `"spim"`: Runs MIPS programs with command-line SPIM. Program name is `<suite_name>.s` unless overridden with "target".
      - `"logisim"`: Simulates a Logisim circuit using our command-line version of Logisim (or Logisim Evolution). Program name is `<suite_name>.circ` unless overridden with "target".
      - `"java"`: Runs compiled Java programs. The java class is the suite name unless overridden with "target" (i.e., uses `<suite_name>.class`).
  - `"test_suites"`: A dictionary of test suite names and test suite content (see below). Required.

Options at the suite-level or higher:
  - `"penalty_logisim_disallowed_components"`: Check for certain components in the Logisim circuit. Represented as a list of penalty/component-lists, where each one has the format `{"penalty": FLOAT, "components": [LIST...], "ignore_subcircuits": [LIST...]}` (and ignore_subcircuits is an optional list of circuit names to skip searching). Optional. Example:

        [
            {
            "penalty": 0.25,
            "components": ["Adder","BitAdder",...],
            "ignore_subcircuits": ["memorylatch", ...]     # this field optional
            }, 
                ...
        ]
  - `"penalty_c_modulo"`: A float to multiply the score of all tests in this suite by if the given C program is caught using the modulo operator. Format is `{"penalty": FLOAT, "file": C_FILENAME}`. Optional.
  - `"penalty_c_math_or_modulo"`: A float to multiply the score of all tests in this suite by if the given C program is caught using the modulo operator *or* including `math.h`. Format is `{"penalty": FLOAT, "file": C_FILENAME}`. Optional.
  - `"tests"`: An array of tests (described below). Required.

Options at the test-level or higher: 
  - `"desc"`: Short description of the test. Required.
  - `"points"`: Point value of this test. Required if "is_grader" is true. 
  - `"args"`: Command line arguments to pass to the program. Optional. Not applicable in "logisim" mode. 
  - `"stdin"`: Redirect stdin from this file. Convention for this this is `"<TEST_DIR>/<SUITENAME>_input_<TESTNUM>.txt"`, but you can do whatever. Optional.
  - `"diff"`: Change the type of diff done to compare expected vs. actual. Choices are `"normal"` (the default) and `"float"`. The float diff will assume lines of `<TOKEN> <FLOAT>` and ensure that floats match within 0.1% -- useful for "PizzaCalc"-type programs.
  - `"penalty_exitcode_nonzero"`: A float to multiply the test score by if the program exits with a non-zero exitcode (including segfaults). Applicable in "exe" mode only. Optional.
  - `"penalty_valgrind"`: A float to multiply the test score by if the program fails to pass valgrind cleanly. This entails a second run of the program. Applicable in "exe" mode only. Optional.
  - `"output_filters"`: A list of text processing functions to apply to output. See the FileFilter class in `hwtest.py` for choices. Optional. Common picks:
    - `"filter_spim"`: Remove the SPIM headers.
    - `"filter_remove_colon_prompts"`: For interactive programs that echo prompts ending in a colon, this will remove them.
    - `"filter_logisim_strip_blank_probes"`: For Logisim, this will remove unnamed probes, allowing them to be used by in the circuit without affecting the tester.
  - `"visibility"`: Selects visibility option of this test in GradeScope JSON. See [GradeScope Autograder documentation](https://gradescope-autograders.readthedocs.io/en/latest/specs/) for details. Optional, default: "visible". Choices as of this writing:
    - `"hidden"`: test case will never be shown to students
    - `"after_due_date"`: test case will be shown after the assignment's due date has passed. If late submission is allowed, then test will be shown only after the late due date.
    - `"after_published"`: test case will be shown only when the assignment is explicitly published from the "Review Grades" page
    - `"visible"` (default): test case will always be shown
  - `"timeout"`: Timeout after which program should be aborted. Optional, default: 10.
  - `"target"`: Override the program/circuit under test (i.e., it's no longer based on suite name). For example, set this at top-level to make all suites work against the same target. Optional.
  - `"spim_command"`: Override the spim commmand. Optional, default: "spim".
  - `"logisim_jar"`: Override the Logisim CLI jar. Optional, default: "logisim_ev_cli.jar".


### Options are hierarchical
All options are hierarchical -- each test will look in its own JSON for a given setting, then the parent suite, then the global context. So, for example, you can set a global timeout of 10 seconds, but tweak it for a given suite to 30 seconds, and set it for a particularly long test in the suite to 90 seconds:

    {
      ...
      "timeout": 10,
      "test_suites": {
        "suite1": {
          "timeout": 30,
          "tests": [
            { "desc": "some test", ...}, 
            { "desc": "long test", ..., "timeout": 90}
          ]
        },
        ...
      }
    }

### Workflow for developing new assignments

To develop a new assignment based on this tool, you'd:
  - Write the reference solution.
  - Make a `tests` directory for student self-testing. If desired, also make another directory for hidden auto-grader tests. In each, write a `settings.json` for test cases.
  - If so inclined, you can manually produce expected output of the form `<TEST_DIR>/<SUITENAME>_expected_<TESTNUM>.txt`. More realistically, you'll probably want to use the hidden -G option, which will take the generated actual output and "bless" it into the expected output. You just gotta be really sure your reference solution is correct :-)
  - If you want GradeScope support, you'll need to write a `setup.sh` script that will set up the GradeScope environment and a `run_autograder` script that will prepare the materials if needed (e.g., compile source code) and run the `hwtest.py` against the autograder test directory for `ALL` suites. [GradeScope autograder documentation is here](https://gradescope-autograders.readthedocs.io/en/latest/specs/), and Tyler Bletsch can provide you reference examples of doing this.

## Developer guide

The `hwtest.py` code is extensively documented, so this section will mainly give a high-level map to orient you, then some tips on where you'd make various kinds of common enhancements.

### Class map

Here's how the various classes interact:

The script creates a `Tester` based on a test directory (default "tests", but can be changed with -t to something else, e.g. "tests-autograder").
The `Tester` wraps the JSON from the `settings.json` file in the test directory and makes some child `Suite` objects in the `tester.suites[]` dictionary (keyed on suite name).

The `Suite` object represents a test suite from the settings JSON, and contains child `Test` objects in the `suite.tests[]` array.

The `Test` object represents a single test. 

The `Tester`, `Suite`, and `Test` classes all inherit from the abstract `JSONWrapper`, which allows lookups of the form `x[k]` to match both JSON of that object (`x.json[k]`) as well as the parent of that object (`x.parent[k]`), recursively.
This is done to support hierarchical settings, so a "timeout" set a the global level can be overridden at the suite level and at the individual test level. 

The result of running one or more test suites via the `Tester` is a `TestResultSet` object which contains `TestResult` objects.

There are some utility classes, too:
 - `CodeCheck`: Checks about the source file are in here as static methods.
 - `Utility`: Misc functions dealing with the system are here as static methods. This includes running processes, finding the java VM, etc.
 - `FileFilter`: Encapsulates named filter operations used to adjust the output of programs under test (e.g. by removing SPIM headers, echoed prompts, unnamed Logisim probes, etc.). 
 - `Diff`: Handles comparing output (expected vs actual), and includes a mode for round-off insensitive diffing (for PizzaCalc-like programs). 

 ### Common enhancements you might want to make

To access any new JSON tag:
 - There's no central tag validator, so adding a new tag is as easy as adding code that will use it. (The downside of this is that superfluous or typo'd tags will be quietly ignored. Tradeoffs...)
 - Do `self['TAGNAME']` or `self.get('TAGNAME',DEFAULT)` to access the tag. Hierarchical lookup will be automatic. 

To add a new test-level check:
 - Add the logic in `Test.run()`.

To add a new suite-level code check:
 - Add the new check to the `CodeCheck` class as a static method.
 - Make a call to your new function in `Suite.check_suite_level_penalties()` based on a new JSON tag of your choice.

To add new forms of output filtering:
 - Just add a new static method starting with `filter_` to `FileFilter`. Pickup of newly added functions is automatic.

To add a new kind of diff support:
 - Add the compare logic as a new static method in the `Diff` class.
 - Add a hook for it in `Diff.apply_diff()`.

To add a new execution mode:
 - Add it to `VALID_TEST_MODES`.
 - Edit `Test.get_command()` to show how the mode can be turned into a shell command.
 - Edit `Test.check_prereq_missing()` to detect if a necessary file or tool is missing.
 - Edit `Suite.get_target()` to show how the object under test is identified by suite name. You could also require an explicit `"target"` here, perhaps?
